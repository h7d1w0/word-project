# word-project

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run this project without build
```
yarn run serve
```
Then go to the website indicated by the output of this command from the browser,
click "Words" tap.


### Show your selected Words
Sorry, very primitive, go to this direcotry: ../src/assets/words.js and freely add/remove
 words in the array.


